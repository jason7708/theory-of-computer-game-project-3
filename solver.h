#pragma once
#include <iostream>
#include <algorithm>
#include <cmath>
#include "board.h"
#include <numeric>
#include <vector>
#include <climits>
#include <cfloat>

class state_type {
public:
	enum type : char {
		before  = 'b',
		after   = 'a',
		illegal = 'i'
	};

public:
	state_type() : t(illegal) {}
	state_type(const state_type& st) = default;
	state_type(state_type::type code) : t(code) {}

	friend std::istream& operator >>(std::istream& in, state_type& type) {
		std::string s;
		if (in >> s) type.t = static_cast<state_type::type>((s + " ").front());
		return in;
	}

	friend std::ostream& operator <<(std::ostream& out, const state_type& type) {
		return out << char(type.t);
	}

	bool is_before()  const { return t == before; }
	bool is_after()   const { return t == after; }
	bool is_illegal() const { return t == illegal; }

public:
	type t;
};

class state_hint {
public:
	state_hint(const board& state) : state(const_cast<board&>(state)) {}

	char type() const { return state.info() ? state.info() + '0' : 'x'; }
	operator board::cell() const { return state.info(); }

public:
	friend std::istream& operator >>(std::istream& in, state_hint& hint) {
		while (in.peek() != '+' && in.good()) in.ignore(1);
		char v; in.ignore(1) >> v;
		hint.state.info(v != 'x' ? v - '0' : 0);
		return in;
	}
	friend std::ostream& operator <<(std::ostream& out, const state_hint& hint) {
		return out << "+" << hint.type();
	}

private:
	board& state;
};


class solver {
public:
	typedef float value_t;

public:
	class answer {
	public:
		answer(value_t min = 0.0/0.0, value_t avg = 0.0/0.0, value_t max = 0.0/0.0) : min(min), avg(avg), max(max) {}
	    friend std::ostream& operator <<(std::ostream& out, const answer& ans) {
	    	return !std::isnan(ans.avg) ? (out << std::setprecision(7) << ans.min << " " << ans.avg << " " << ans.max) : (out << "-1");
		}
	public:
		int min;
		value_t avg;
		int max;
	};
public:
	class data {
	public:
		data() : type(static_cast<state_type::type>('a')), last(0){
			
			for(int i=0;i<6;i++){
				b(i) = 0;
			}

			bag[0] = 1;
			bag[1] = 1;
			bag[2] = 1;
		}
	public:
		state_type type;
		board b;
		int last;
		bool bag[3];
	};
public:
	solver(const std::string& args) {
		// TODO: explore the tree and save the result
		
//		std::cout << "feel free to display some messages..." << std::endl;
		after_table.resize(531441, std::vector<answer>(16)); //531441 = 9^6
		befor_table.resize(531441, std::vector<answer>(4));
		
		for(int i=0;i<531441;i++){
			for(int j=0;j<16;j++){
				after_table[i][j].min = -1;
				after_table[i][j].avg = 0.0/0.0;
				after_table[i][j].max = -1;
				
			}
		}
		for(int i=0;i<531441;i++){
			for(int j=0;j<4;j++){
				befor_table[i][j].min = -1;
				befor_table[i][j].avg = 0.0/0.0;
				befor_table[i][j].max = -1;
				
			}
		}
		data init;
		for(int i=0;i<3;i++){ // put 1 or 2 or 3
			for(int j=0;j<3;j++){ // hint for next 1 or 2 or 3
				if(i == j){
					continue;
				}
				for(int k=0;k<6;k++){ // place at 0~5
					for(int l=0;l<4;l++){ //last action
						//std::cout << i << " " << j<<" "<<k<<" "<<l<<std::endl;
						data next(init);
						next.type = static_cast<state_type::type>('b');
						next.b.info(j+1);
						next.last = l;
						next.bag[i] = 0;
						next.b.place(k, i+1);
						tree_dfs(next);
					}
				}
			}
		}
	}
	answer tree_dfs (data d) {
		//d.b.print_board();
		//std::cout << d.type.is_after() << "\n" << d.bag[0] << d.bag[1] << d.bag[2]<<"\n";
		if(d.type.is_after()){ //evil turn
			//if already in table
			if(after_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.last*4+d.b.info()].min != -1){
				return after_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.last*4+d.b.info()];
			}
			int min = INT_MAX, max = INT_MIN;
			value_t avg = 0.0;
			int n=0; //how many ways
			int num;
			num = d.b.info(); //place what hint is
			std::array<int, 3> space;
			switch (d.last){
				case 0://up
					space = {3, 4, 5};
					break;
				case 1://right
					space = {0, 3, -1};
					break;
				case 2://down
					space = {0, 1, 2};
					break;
				case 3://left
					space = {2, 5, -1};
					break;
				default:
					std::cout << "error\n";
					break;
			}
			//std::cout << "87db5\n";
			for(int i=0;i<3;i++){
				if(space[i] == -1){
					continue;
				}
				if(d.b(space[i]) != 0){ //already have tile
					continue;
				}
				data next(d);
				next.b.place(space[i], num);
				next.bag[num-1] = false;
				if(next.bag[0] == false && next.bag[1] == false && next.bag[2] == false){
					next.bag[0] = true;
					next.bag[1] = true;
					next.bag[2] = true;
				}
				next.type = static_cast<state_type::type>('b');
				
				for(int j=1;j<=3;j++){ //all kinds of hint
					if(next.bag[j-1] == false){
						continue;
					}
					next.b.info(j);
					answer a;
					a = tree_dfs(next);
					if(a.min < min){
						min = a.min;
					}
					if(a.max > max){
						max = a.max;
					}
					avg += a.avg;
					n++;
				}
			}
			//std::cout << "87db6\n";
			avg /= n;
			answer a;
			a.min = min;
			a.avg = avg;
			a.max = max;
			after_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.last*4+d.b.info()] = a;
			return a;

		}
		else if(d.type.is_before()){
			//if already in table
			if(befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()].min != -1){
				return befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()];
			}
			//std::cout << "87db2\n";
			bool leaf = true;
			int min = -1, max = -1;
			value_t avg, tmp = FLT_MIN;
			for(int i=0;i<4;i++){ //up right down left
				data next(d);
				next.type = static_cast<state_type::type>('a');
				if(next.b.slide(i) == -1){
					continue;
				}
				leaf = false;
				next.last = i;
				answer a;
				//std::cout << "87db777\n";
				a = tree_dfs(next);
				if(a.avg > tmp){
					tmp = a.avg;
					avg = a.avg;
					min = a.min;
					max = a.max;
				}
			}
			//std::cout << "87db3\n";
			if(leaf == true){
				int r=0;
				r = d.b.compute_reward();
				befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()].min = r;
				befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()].avg = r;
				befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()].max = r;
				answer a;
				a.min = r;
				a.max = r;
				a.avg = r;
				return a;
			}
			else{
				answer a;
				a.min = min;
				a.avg = avg;
				a.max = max;
				befor_table[d.b(0)+d.b(1)*9+d.b(2)*9*9+d.b(3)*9*9*9+d.b(4)*9*9*9*9+d.b(5)*9*9*9*9*9][d.b.info()] = a;
				return a;
			}
		}
		else{
			answer illegal;
			illegal.min = -1;
			illegal.avg = 0.0;
			illegal.max = -1;
			std::cout << "error\n";
			return illegal;
		}
	}
	answer solve(const board& state, state_type type) {
		// TODO: find the answer in the lookup table and return it
		//       do NOT recalculate the tree at here

		// to fetch the hint (if type == state_type::after, hint will be 0)
//		board::cell hint = state_hint(state);

		// for a legal state, return its three values.
//		return { min, avg, max };
		// for an illegal state, simply return {}
		for(int i=0;i<=5;i++){
			if(state(i)!=0 && state(i) != 1 && state(i) != 2 && state(i) != 3 && state(i) != 4 && state(i) != 5 && state(i) != 6 && state(i) != 7 && state(i) != 8){
				return {};
			}
		}
		if(type.is_before()){
			return befor_table[state(0)+state(1)*9+state(2)*9*9+state(3)*9*9*9+state(4)*9*9*9*9+state(5)*9*9*9*9*9][state.info()];
		}
		if(type.is_after()){
			for(int i=0;i<4;i++){
				if(after_table[state(0)+state(1)*9+state(2)*9*9+state(3)*9*9*9+state(4)*9*9*9*9+state(5)*9*9*9*9*9][i*4+state.info()].min == -1){
					continue;
				}
				return after_table[state(0)+state(1)*9+state(2)*9*9+state(3)*9*9*9+state(4)*9*9*9*9+state(5)*9*9*9*9*9][i*4+state.info()];
			}
		}
		return {};
	}
	
private:
	// TODO: place your transposition table here
	std::vector<std::vector<answer> > befor_table;
	std::vector<std::vector<answer> > after_table;
};
