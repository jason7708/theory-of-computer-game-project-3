#pragma once
#include <array>
#include <iostream>
#include <iomanip>
#include <cmath>

/**
 * array-based board for 2048
 *
 * index (1-d form):
 *  (0)  (1)  (2)  (3)
 *  (4)  (5)  (6)  (7)
 *  (8)  (9) (10) (11)
 * (12) (13) (14) (15)
 *
 */
class board {
public:
	typedef uint32_t cell;
	typedef std::array<cell, 3> row;
	typedef std::array<row, 2> grid;
	typedef uint64_t data;
	typedef int reward;

public:
	board() : tile(), attr(0) {}
	board(const grid& b, data v = 0) : tile(b), attr(v) {}
	board(const board& b) = default;
	board& operator =(const board& b) = default;

	operator grid&() { return tile; }
	operator const grid&() const { return tile; }
	row& operator [](unsigned i) { return tile[i]; }
	const row& operator [](unsigned i) const { return tile[i]; }
	cell& operator ()(unsigned i) { return tile[i / 3][i % 3]; }
	const cell& operator ()(unsigned i) const { return tile[i / 3][i % 3]; }

	data info() const { return attr; } //get
	data info(data dat) { data old = attr; attr = dat; return old; }  //give

public:
	bool operator ==(const board& b) const { return tile == b.tile; }
	bool operator < (const board& b) const { return tile <  b.tile; }
	bool operator !=(const board& b) const { return !(*this == b); }
	bool operator > (const board& b) const { return b < *this; }
	bool operator <=(const board& b) const { return !(b < *this); }
	bool operator >=(const board& b) const { return !(*this < b); }

public:

	/**
	 * place a tile (index value) to the specific position (1-d form index)
	 * return 0 if the action is valid, or -1 if not
	 */
	reward place(unsigned pos, cell tile) {
		if (pos >= 6) return -1;
		if (tile != 1 && tile != 2 && tile != 3) return -1;
		operator()(pos) = tile;
		if(tile == 3){
			return 3;
		}
		return 0;
	}
	reward compute_reward(void){
		int sum = 0;
		for(int r=0; r<2; r++){
			auto&row = tile[r];
			for(int c=0; c<3; c++) {
				if(row[c] > 2 && row[c] <= 14)
				sum += pow(3, row[c]-2);
			}
		}
		return sum;
	}
	void print_board(){
		std::cout << tile[0][0] <<" "<<tile[0][1]<<" "<<tile[0][2]<<'\n'<<tile[1][0]<<" "<<tile[1][1]<<" "<<tile[1][2]<<'\n'<<"8===D"<<'\n';
	}
	/**
	 * apply an action to the board
	 * return the reward of the action, or -1 if the action is illegal
	 */
	reward slide(unsigned opcode) {
		switch (opcode & 0b11) {
		case 0: return slide_up();
		case 1: return slide_right();
		case 2: return slide_down();
		case 3: return slide_left();
		default: return -1;
		}
	}

	reward slide_left() {
		board prev = *this;
		reward score = 0;
		//compute reward
		int sum_old=0, sum_new=0;
		sum_old = compute_reward();
		//^^^^^^^^^^^^^^^

		for(int r=0; r<2; r++) {
			auto& row = tile[r];
			int left = 0, com = 0; 
			for(int c=0; c<2; c++) {
				if(row[c] == 0) {
					left = 1;
				}
				if(left != 1) {
					if((row[c] == 1 && row[c+1] == 2)||(row[c] == 2 && row[c+1] == 1)||((row[c] == row[c+1])&&(row[c] != 0)&&(row[c] != 1)&&(row[c] != 2)&&(row[c] != 14))) {
						com = 1;
						left = 1;
						if(row[c]==2&&row[c+1]==1) {
						com++;
						}
					}
				}
				if(left == 1) {
					row[c] = row[c+1] + com;
					com = 0;
					if(c==1) {
						row[c+1] = 0;
					}
				}
			}
		}
		//^^^^^^^^^^^^^^^^^^^^^^^^
		sum_new = compute_reward();
		score = sum_new - sum_old;
		//^^^^^^^^^^^^^^^^^^^^^^^^
		return (*this != prev) ? score : -1;
	}
	reward slide_right() {
		reflect_horizontal();
		reward score = slide_left();
		reflect_horizontal();
		return score;
	}
	reward slide_up() {
		//4x4
		/* 
		rotate_right();
		reward score = slide_right();
		rotate_left();
		return score;
		*/
		//2x3
		board prev = *this;
		reward score = 0;
		int sum_old=0, sum_new=0;
		//compute reward
		sum_old = compute_reward();
		//^^^^^^^^^^^^^^^

		for(int c=0;c<3;c++){
			if((tile[0][c] == 1 && tile[1][c] == 2)||(tile[0][c] == 2 && tile[1][c] == 1)){
				tile[0][c] = 3;
				tile[1][c] = 0;
			}
			else if((tile[0][c] == tile[1][c]) && (tile[0][c] != 0) && (tile[0][c] != 1) && (tile[0][c] != 2)){
				tile[0][c] += 1;
				tile[1][c] = 0;
			}
			else if(tile[0][c] == 0){
				tile[0][c] = tile[1][c];
				tile[1][c] = 0;
			}
			/*else{
				tile[0][c] = tile[0][c];
				tile[1][c] = tile[1][c];
			}*/
		}

		sum_new = compute_reward();
		score = sum_new - sum_old;
		//^^^^^^^^^^^^^^^^^^^^^^^^
		return (*this != prev) ? score : -1;
	}
	reward slide_down() {
		/*
		rotate_right();
		reward score = slide_left();
		rotate_left();
		return score;
		*/
		board prev = *this;
		reward score = 0;
		int sum_old=0, sum_new=0;
		//compute reward
		sum_old = compute_reward();
		//^^^^^^^^^^^^^^^

		for(int c=0;c<3;c++){
			if((tile[0][c] == 1 && tile[1][c] == 2)||(tile[0][c] == 2 && tile[1][c] == 1)){
				tile[1][c] = 3;
				tile[0][c] = 0;
			}
			else if((tile[0][c] == tile[1][c]) && (tile[0][c] != 0) && (tile[0][c] != 1) && (tile[0][c] != 2)){
				tile[1][c] += 1;
				tile[0][c] = 0;
			}
			else if(tile[1][c] == 0){
				tile[1][c] = tile[0][c];
				tile[0][c] = 0;
			}
			/*else{
				tile[0][c] = tile[0][c];
				tile[1][c] = tile[1][c];
			}*/
		}

		sum_new = compute_reward();
		score = sum_new - sum_old;
		//^^^^^^^^^^^^^^^^^^^^^^^^
		return (*this != prev) ? score : -1;
	}
/*
	void transpose() {
		for (int r = 0; r < 4; r++) {
			for (int c = r + 1; c < 4; c++) {
				std::swap(tile[r][c], tile[c][r]);
			}
		}
	}
*/
	void reflect_horizontal() {
		for (int r = 0; r < 2; r++) {
			std::swap(tile[r][0], tile[r][2]);
		}
	}

	void reflect_vertical() {
		for (int c = 0; c < 3; c++) {
			std::swap(tile[0][c], tile[1][c]);
		}
	}

	/**
	 * rotate the board clockwise by given times
	 */
	/*
	void rotate(int r = 1) {
		switch (((r % 4) + 4) % 4) {
		default:
		case 0: break;
		case 1: rotate_right(); break;
		case 2: reverse(); break;
		case 3: rotate_left(); break;
		}
	}
	*/
	//void rotate_right() { transpose(); reflect_horizontal(); } // clockwise
	//void rotate_left() { transpose(); reflect_vertical(); } // counterclockwise
	//void reverse() { reflect_horizontal(); reflect_vertical(); }

public:
	friend std::ostream& operator <<(std::ostream& out, const board& b) {
		for (int i = 0; i < 6; i++) {
			if(b.valid[i] == true){
				if(b(i)>3){
					out << std::setw(std::min(i, 1)) << "" << ((1<<(b(i)-3))*3);
				}
				else{
					out << std::setw(std::min(i, 1)) << "" << b(i);
				}
			}
			else{
				out << std::setw(std::min(i, 1)) << "" << b(i);
			}
		}
		return out;
	}
	friend std::istream& operator >>(std::istream& in, board& b) {
		for (int i = 0; i < 6; i++) {
			while (!std::isdigit(in.peek()) && in.good()) in.ignore(1);
			int trash;
			in >> trash;
			//std::cout << trash << "RRR\n";
			if(trash!= 0&&trash!=1&&trash!=2&&trash!=3&&trash!=6&&trash!=12&&trash!=24&&trash!=48&&trash!=96){
				std::cout << "invalid input\n" << trash << "\n";
				b(i) = trash;
				b.valid[i] = false;
				continue;
			}
			b.valid[i] = true;
			if(trash>3){
				trash /= 3;
				trash = std::log2(trash);
				trash += 3;
				b(i) = trash;
			}
			else{
				b(i) = trash;
			}
		}
		return in;
	}

private:
	grid tile;
	data attr;
	bool valid[6];
};
